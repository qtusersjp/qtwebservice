import QtWebService.Config 0.1

Config {
    listen: Listen {
        address: 'localhost'
        port: 8080
    }

    contents: {
        '/': ':/root/'
    }

    daemons: [
        ':/daemons/chatdaemon.qml'
    ]

    imports: []

    cache: {
        'qml': true
    }

    deflate: {
        'video/*': false
        , 'image/*': false
    }
}
