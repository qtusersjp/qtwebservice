import QtWebService.Image 1.0

Image {
    id: root
    width: 100
    height: 100
    contentType: 'image/png'
    source: './qt-logo.png'

    Text {
        x: (root.width - width) / 2
        y: 70
        text: 'Hello World'
    }
}
