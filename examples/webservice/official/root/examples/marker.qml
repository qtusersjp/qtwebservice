import QtWebService.Image 1.0

Image {
    id: root
    width: 32
    height: 32
    contentType: 'image/png'
    source: './marker.png'

    Text {
        x: (root.width - width) / 2
        y: 22
        text: (new Date).getSeconds()
    }
}
