/* Copyright (c) 2012 QtWebService Project.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the QtWebService nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL QTWEBSERVICE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import QtWebService.HTML 5.0
import QtWebService.Bootstrap 2.3 as Bootstrap

QtWebServicePageTemplate {
    Head {
        Title { text: 'QtWebService - getting started' }
    }
    data_spy: 'scroll'
    data_target: '.bs-docs-sidebar'

    Bootstrap.Row {
        Bootstrap.Span3 {
            _class: 'bs-docs-sidebar'
            Bootstrap.NavList {
                _class: 'bs-docs-sidenav affix'
                Bootstrap.NavItem {
                    a.href: '#qt'
                    Bootstrap.IconChevronRight {}
                    Text { text: '1. Qt 5' }
                }
                Bootstrap.NavItem {
                    a.href: '#qthttpserver'
                    Bootstrap.IconChevronRight {}
                    Text { text: '2. QtHttpServer' }
                }
                Bootstrap.NavItem {
                    a.href: '#qtwebservice'
                    Bootstrap.IconChevronRight {}
                    Text { text: '3. QtWebService' }
                }
                Bootstrap.NavItem {
                    a.href: '#helloworld'
                    Bootstrap.IconChevronRight {}
                    Text { text: '4. Hello World' }
                }
                Bootstrap.NavItem {
                    a.href: '#configuration'
                    Bootstrap.IconChevronRight {}
                    Text { text: '5. Configuration' }
                }
            }
        }
        Bootstrap.Span9 {
            Section {
                _id: 'qt'
                Bootstrap.PageHeader {
                    H1 { text: '1. Qt' }
                }
                Bootstrap.Lead {
                    Text { text: 'download and install Qt 5.6 or later from ' }
                    A { href: 'http://www.qt.io/download-open-source/'; target: '_blank'; text: 'qt.io' }
                }
            }
            Section {
                _id: 'qtwebservice'
                Bootstrap.PageHeader {
                    H1 { text: '2. QtHttpServer' }
                }
                Bootstrap.RowFluid {
                    Bootstrap.Span6 {
                        H2 { text: 'Building from source' }
                        Pre {
                            Kbd { text: '$ git clone <a href="http://git.qt-users.jp/git/?p=codereview/qthttpserver.git;a=tree" target="_blank">git://git.qt-users.jp/codereview/qthttpserver.git</a>\n' }
                            Kbd { text: "$ mkdir build\n" }
                            Kbd { text: "$ cd $_\n" }
                            Kbd { text: "$ qt5/bin/qmake\n" }
                            Kbd { text: "$ make\n" }
                            Kbd { text: "$ (sudo) make install\n" }
                            Kbd { text: "$ ./examples/hello/httphelloserver\n" }
                        }
                        P {
                            Text { text: 'open ' }
                            A { href: 'http://localhost:8080/'; target: '_blank'; text: 'localhost:8080' }
                        }
                    }
                }
            }
            Section {
                _id: 'qtwebservice'
                Bootstrap.PageHeader {
                    H1 { text: '3. QtWebService' }
                }
                Bootstrap.RowFluid {
                    Bootstrap.Span6 {
                        H2 { text: 'Build from source' }
                        Pre {
                            Kbd { text: '$ git clone <a href="http://git.qt-users.jp/git/?p=codereview/qtwebservice.git;a=tree" target="_blank">git://git.qt-users.jp/codereview/qtwebservice.git</a>\n' }
                            Kbd { text: "$ cd qtwebservice\n" }
                            Kbd { text: "$ qt5/bin/qmake\n" }
                            Kbd { text: "$ make\n" }
                            Kbd { text: "$ (sudo) make install\n" }
                            Kbd { text: "$ ./examples/webservice/official/official\n" }
                            Text { text: 'qtwebservice is running on 8080' }
                        }
                        P {
                            Text { text: 'open ' }
                            A { href: 'http://localhost:8080/'; target: '_blank'; text: 'localhost:8080' }
                        }
                    }
                }
            }
            Section {
                _id: 'helloworld'
                Bootstrap.PageHeader {
                    H1 { text: '4. Hello World' }
                }
                Bootstrap.RowFluid {
                    Bootstrap.Span6 {
                        H2 { text: 'helloworld.pro' }
                        Pre {
                            text: "TEMPLATE = app
TARGET = helloworld
QT = webservice
SOURCES += main.cpp
RESOURCES += main.qrc"
                        }
                    }
                    Bootstrap.Span6 {
                        H2 { text: 'main.cpp' }
                        Pre {
                            text: "#include &lt;QtCore/QCoreApplication&gt;
#include &lt;QtWebService/QWebServiceEngine&gt;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    QWebServiceEngine engine(QStringLiteral(\":/config.qml\"));
    engine.start();

    return app.exec();
}"
                        }
                    }
                    Bootstrap.Span6 {
                        H2 { text: 'main.qrc' }
                        Pre {
                            text: "&lt;RCC&gt;
    &lt;qresource prefix=\"/\"&gt;
        &lt;file&gt;config.qml&lt;/file&gt;
        &lt;file&gt;root/index.qml&lt;/file&gt;
    &lt;/qresource&gt;
&lt;/RCC&gt;
"
                        }
                    }
                    Bootstrap.Span6 {
                        H2 { text: 'config.qml' }
                        Pre {
                            text: "import QtWebService.Config 0.1

Config {
    listen.address: '*'
    listen.port: 8080
    server.name: 'QtWebService'

    contents: {
        '/': './root/',
    }

    cache: {
        'qml': true
    }

    deflate: {
        'video/*': false
        , 'image/*': false
    }
}"
                        }
                    }
                    Bootstrap.Span6 {
                        H2 { text: 'root/index.qml' }
                        Pre {
                            text: "import QtWebService.HTML 5.0

Html {
    Head { title: 'QtWebService' }
    Body {
        H1 { text: 'QtWebService' }
        P { text: '...' }
    }
}"
                        }
                    }
                    Bootstrap.Span6 {
                        H2 { text: 'Build &amp; Run' }
                        Pre {
                            Kbd { text: "$ qmake &amp;&amp; make -j4 &amp;&amp; ./helloworld" }
                        }
                    }
                }
            }
            Section {
                _id: 'configuration'
                Bootstrap.PageHeader {
                    H1 { text: '5. Configuration' }
                }
                Bootstrap.RowFluid {
                    Bootstrap.Span6 {
                        H2 { text: 'Default configuration' }
                        Pre {
                            text: "import QtWebService.Config 0.1

Config {
    listen: Listen {
        address: '*'
        port: 8080
    }

    server.name: 'QtWebService'

    contents: {}

    daemons: []

    imports: []

    cache: {}

    deflate: {}
}"
                        }
                    }
                    Bootstrap.Span3 {
                        H3 { text: 'listen.address' }
                        Dl {
                            _class: 'well'
                            Dt { text: '"*"' }
                            Dd { text: 'listen any address' }
                            Dt { text: '"localhost"' }
                            Dd { text: 'listen localhost only' }
                            Dt { text: '"nnn.nnn.nnn.nnn"' }
                            Dd { text: 'listen an ip address' }
                        }
                        H3 { text: 'listen.port' }
                        P {
                            _class: 'well'
                            Text { text: 'port number' }
                        }
                        H3 { text: 'contents' }
                        P {
                            _class: 'well'
                            Text { text: 'path -> document root pair' }
                        }
                        H3 { text: 'daemons' }
                        P {
                            _class: 'well'
                            Text { text: 'background services' }
                        }
                    }
                    Bootstrap.Span3 {
                        H3 { text: 'offlineStoragePath' }
                        P {
                            _class: 'well'
                            Text { text: 'root path for offline storage' }
                        }
                        H3 { text: 'imports' }
                        P {
                            _class: 'well'
                            Text { text: 'additional import path for QML' }
                        }
                        H3 { text: 'cache' }
                        P {
                            _class: 'well'
                            Text { text: 'if false, qtwebservice generates html from qml everytime' }
                        }
                        H3 { text: 'deflate' }
                        P {
                            _class: 'well'
                            Text { text: 'mime types not to be compressed' }
                        }
                    }
                }
            }
        }
    }
}
