TEMPLATE = app
TARGET = official
QT = webservice
qtHaveModule(gui): QT += gui
CONFIG += install_ok
SOURCES += main.cpp
RESOURCES += main.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/webservice/official
INSTALLS += target
