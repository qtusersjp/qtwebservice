import QtWebService.Config 0.1

Config {
    listen.address: '*'
    listen.port: 8080
    server.name: 'QtWebService'

    contents: {
        '/': '/webroot/',
    }

    cache: {
        'qml': true
    }

    deflate: {
        'video/*': false
        , 'image/*': false
    }
}
