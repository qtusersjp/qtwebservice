FROM alpine:3.6
MAINTAINER Tasuku Suzuki

ENV QT5_REPOSITORY git://git.qt-users.jp/mirror/qt/qt5.git
# ENV QT5_REPOSITORY git://code.qt.io/qt/qt5.git

# ENV QT5_OPTS -opensource -confirm-license -prefix /opt/qt/ -make libs -nomake tools -no-gui -no-glib -release -no-feature-dbus -no-feature-concurrent -no-feature-statemachine -no-feature-animation -no-feature-translation
ENV QT5_OPTS -opensource -confirm-license -prefix /opt/qt/ -make libs -nomake tools -no-gui -no-glib -release -no-qml-debug -no-feature-dbus -no-feature-concurrent -no-feature-testlib -no-feature-statemachine -no-feature-animation -no-feature-translation -no-feature-udpsocket -no-feature-networkproxy -no-feature-bearermanagement -no-feature-localserver

RUN set -x  && \
    apk add --no-cache libstdc++ && \
    apk add --no-cache --virtual .builddep alpine-sdk perl python linux-headers zlib-dev && \
    mkdir -p /root/qt5 && cd /root/qt5 && \
    git clone ${QT5_REPOSITORY} --branch 5.10 && \
    cd qt5 && \
    git submodule update --init qtbase/ qtdeclarative/ && \
    mkdir /root/qt5/build && cd /root/qt5/build && \
    ../qt5/configure ${QT5_OPTS} && \
    NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    make -j${NPROC} && \
    make -j${NPROC} install && \
    mkdir /root/qthttpserver && cd /root/qthttpserver && \
    git clone git://git.qt-users.jp/codereview/qthttpserver && \
    mkdir /root/qthttpserver/build && cd /root/qthttpserver/build && \
    /opt/qt/bin/qmake -r ../qthttpserver/ && \
    NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    make -j${NPROC} && \
    make -j${NPROC} install && \
    mkdir /root/qtwebservice && cd /root/qtwebservice && \
    git clone git://git.qt-users.jp/codereview/qtwebservice && \
    mkdir /root/qtwebservice/build && cd /root/qtwebservice/build && \
    /opt/qt/bin/qmake -r ../qtwebservice/ && \
    NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    make -j${NPROC} && \
    make -j${NPROC} install && \
    make -j${NPROC} sub-tools && \
    make -j${NPROC} sub-tools-install_subtargets && \
    cd /root && \
    rm -rf qt5 qthttpserver qtwebservice && \
    cd /opt/qt/ && \
    rm bin/fixqt4headers.pl bin/qlalr bin/qml* bin/rcc bin/moc bin/qmake bin/syncqt.pl && \
    rm -rf doc include mkspecs && \
    rm -rf plugins/bearer plugins/qmltooling plugins/sqldrivers && \
    rm -rf lib/cmake lib/libQt5Bootstrap* lib/libQt5Concurrent* lib/libQt5PacketProtocol.* lib/libQt5QmlDe* lib/libQt5Test.* lib/pkgconfig && \
    apk del .builddep && \
    mkdir mkdir /webroot

ADD config.qml /opt/qt/bin/
VOLUME ["/webroot"]

EXPOSE 8080

CMD ["/opt/qt/bin/qtwebservice", "--config", "/opt/qt/bin/config.qml"]
# ENTRYPOINT ["/opt/qt/bin/qtwebservice", "--config", "/opt/qt/bin/config.qml"]
