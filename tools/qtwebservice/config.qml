import QtWebService.Config 0.1

Config {
    listen: Listen {
        address: 'localhost'
        port: 8080
    }

    cache: {
        'qml': true
    }

    deflate: {
        'video/*': false
        , 'image/*': false
    }
}
