TARGET = Utils

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = UtilsPlugin
load(qt_plugin)

HEADERS += \
    utilsplugin.h \
    repeater.h \
    config.h \
    server.h \
    client.h \
    recursive.h

SOURCES += \
    repeater.cpp \
    config.cpp \
    server.cpp \
    client.cpp \
    recursive.cpp

OTHER_FILES += Utils.json
