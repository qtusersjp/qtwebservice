TARGET = XML

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = XmlPlugin
load(qt_plugin)

HEADERS += \
    xmlplugin.h \
    xmltag.h \
    xmlcomment.h

SOURCES += \
    xmltag.cpp \
    xmlcomment.cpp

OTHER_FILES += XML.json
