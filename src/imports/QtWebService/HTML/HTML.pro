TARGET = HTML

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = HtmlPlugin
load(qt_plugin)

HEADERS += \
    htmlplugin.h

RESOURCES += \
    HTML.qrc

OTHER_FILES += HTML.json
