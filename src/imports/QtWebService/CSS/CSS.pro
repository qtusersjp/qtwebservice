TARGET = CSS

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = CssPlugin
load(qt_plugin)

HEADERS += \
    cssplugin.h \
    cssrule.h

SOURCES += \
    cssrule.cpp

RESOURCES += \
    CSS.qrc

OTHER_FILES += CSS.json
