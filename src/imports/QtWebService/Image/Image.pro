TARGET = Image

QT = webservice gui

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = ImagePlugin
load(qt_plugin)

HEADERS += \
    imageplugin.h \
    image.h \
    abstractdrawable.h \
    text.h

SOURCES += \
    image.cpp \
    abstractdrawable.cpp \
    text.cpp

OTHER_FILES += Image.json
