/* Copyright (c) 2012-2017 QtWebService Project.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the QtWebService nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL QTWEBSERVICE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ABSTRACTDRAWABLE_H
#define ABSTRACTDRAWABLE_H

#include <QtWebService/QWebServiceAbstractHttpObject>

class QPainter;

class AbstractDrawable : public QWebServiceAbstractHttpObject
{
    Q_OBJECT

    Q_PROPERTY(QString contentType READ contentType WRITE setContentType NOTIFY contentTypeChanged)
    Q_PROPERTY(int x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(int y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(int z READ z WRITE setZ NOTIFY zChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int height WRITE height WRITE setHeight NOTIFY heightChanged)
public:
    explicit AbstractDrawable(QObject *parent = 0);
    ~AbstractDrawable();

    QString contentType() const;
    int x() const;
    int y() const;
    int z() const;
    int width() const;
    int height() const;

    virtual void draw(QPainter *painter) = 0;
    QByteArray out() override;

public slots:
    void setContentType(const QString &contentType);
    void setX(int x);
    void setY(int y);
    void setZ(int z);
    void setWidth(int width);
    void setHeight(int height);

signals:
    void contentTypeChanged(const QString &contentType);
    void xChanged(int x);
    void yChanged(int y);
    void zChanged(int z);
    void widthChanged(int width);
    void heightChanged(int height);


private:
    Q_DISABLE_COPY(AbstractDrawable)
    class Private;
    Private *d;
};

#endif // ABSTRACTDRAWABLE_H
