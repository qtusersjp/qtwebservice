#include "abstractdrawable.h"

#include <QtCore/QDebug>
#include <QtCore/QBuffer>
#include <QtGui/QImage>
#include <QtGui/QImageWriter>
#include <QtGui/QPainter>

class AbstractDrawable::Private
{
public:
    Private();
    QString contentType;
    int x;
    int y;
    int z;
    int width;
    int height;
};

AbstractDrawable::Private::Private()
    : x(0)
    , y(0)
    , z(0)
    , width(0)
    , height(0)
{
}

AbstractDrawable::AbstractDrawable(QObject *parent)
    : QWebServiceAbstractHttpObject(parent)
    , d(new Private)
{
}

AbstractDrawable::~AbstractDrawable()
{
    delete d;
}

QString AbstractDrawable::contentType() const
{
    return d->contentType;
}

void AbstractDrawable::setContentType(const QString &contentType)
{
    if (d->contentType == contentType) return;
    d->contentType = contentType;
    emit contentTypeChanged(contentType);
}

int AbstractDrawable::x() const
{
    return d->x;
}

void AbstractDrawable::setX(int x)
{
    if (d->x == x) return;
    d->x = x;
    emit xChanged(x);
}

int AbstractDrawable::y() const
{
    return d->y;
}

void AbstractDrawable::setY(int y)
{
    if (d->y == y) return;
    d->y = y;
    emit yChanged(y);
}

int AbstractDrawable::z() const
{
    return d->z;
}

void AbstractDrawable::setZ(int z)
{
    if (d->z == z) return;
    d->z = z;
    emit zChanged(z);
}

int AbstractDrawable::width() const
{
    return d->width;
}

void AbstractDrawable::setWidth(int width)
{
    if (d->width == width) return;
    d->width = width;
    emit widthChanged(width);
}

int AbstractDrawable::height() const
{
    return d->height;
}

void AbstractDrawable::setHeight(int height)
{
    if (d->height == height) return;
    d->height = height;
    emit heightChanged(height);
}

QByteArray AbstractDrawable::out()
{
    QByteArray ret;

    QImage image(width(), height(), QImage::Format_ARGB32);
    image.fill(Qt::transparent);
    QPainter painter(&image);
    draw(&painter);
    for (QObject *content : contentsList()) {
        AbstractDrawable *drawable = qobject_cast<AbstractDrawable *>(content);
        if (drawable)
            drawable->draw(&painter);
    }
    painter.end();

    QBuffer buffer(&ret);
    buffer.open(QBuffer::WriteOnly);
    QImageWriter writer(&buffer, contentType().section(QLatin1Char('/'), 1, 1).toLocal8Bit());
    if (!writer.write(image)) {
        qWarning() << writer.errorString();
    }
    buffer.close();
    return ret;
}
