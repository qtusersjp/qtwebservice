TARGET = Cache

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = CachePlugin
load(qt_plugin)


HEADERS += \
    cacheobject.h \
    cacheplugin.h

SOURCES += \
    cacheobject.cpp

OTHER_FILES += Cache.json
