TARGET = Process

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = ProcessPlugin
load(qt_plugin)

HEADERS += \
    process.h \
    processplugin.h

SOURCES += \
    process.cpp

OTHER_FILES += Process.json
