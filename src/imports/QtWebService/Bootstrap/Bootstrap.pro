TARGET = Bootstrap

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = BootstrapPlugin
load(qt_plugin)

HEADERS += \
    bootstrapplugin.h

RESOURCES += \
    Bootstrap.qrc

OTHER_FILES += \
    Bootstrap.json \
    NavCollapse.qml \
    ABtn.qml \
    ABtnPrimary.qml \
    Span1.qml \
    Span2.qml \
    Span5.qml \
    Span12.qml \
    LabelSuccess.qml \
    LabelWarning.qml \
    LabelImportant.qml \
    LabelInfo.qml \
    LabelInverse.qml \
    Thumbnails.qml \
    NavDropdown.qml \
    Divider.qml \
    NavbarForm.qml
