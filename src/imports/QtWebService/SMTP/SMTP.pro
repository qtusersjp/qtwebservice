TARGET = SMTP

QT = webservice network

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = SmtpPlugin
load(qt_plugin)

HEADERS += \
    smtp.h \
    smtpplugin.h

SOURCES += \
    smtp.cpp

OTHER_FILES += SMTP.json
