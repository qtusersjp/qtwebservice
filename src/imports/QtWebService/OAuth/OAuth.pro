TARGET = OAuth

QT = webservice network

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = OAuthPlugin
load(qt_plugin)

HEADERS += \
    oauth.h \
    hmac_sha1.h \
    oauthplugin.h

SOURCES += \
    hmac_sha1.cpp \
    oauth.cpp

OTHER_FILES += OAuth.json
