TARGET = JSON

QT = webservice

PLUGIN_TYPE = webservice-imports
PLUGIN_CLASS_NAME = JsonPlugin
load(qt_plugin)

HEADERS += \
    jsonobject.h \
    jsonplugin.h

SOURCES += \
    jsonobject.cpp

OTHER_FILES += JSON.json
