TARGET = deflate

QT = webservice

PLUGIN_TYPE = webservice-protocolhandler
PLUGIN_CLASS_NAME = HttpPlugin
load(qt_plugin)

HEADERS += \
    httpplugin.h \
    httphandler.h

SOURCES += \
    httphandler.cpp
