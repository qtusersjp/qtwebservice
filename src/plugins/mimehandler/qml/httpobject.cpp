/* Copyright (c) 2012 QtWebService Project.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the QtWebService nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL QTWEBSERVICE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "httpobject.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QTemporaryFile>

#include <QtHttpServer/QHttpRequest>

class HttpFileData::Private
{
public:
    Private(QHttpFileData *data);

    QTemporaryFile file;
    QString fileName;
    QString contentType;
};

HttpFileData::Private::Private(QHttpFileData *data)
    : fileName(data->fileName())
    , contentType(data->contentType())
{
    if (file.open()) {
        file.write(data->readAll());
        file.close();
    } else {
        qWarning() << file.errorString();
    }
}

HttpFileData::HttpFileData(QHttpFileData *data, QObject *parent)
    : QObject(parent)
    , d(new Private(data))
{
}

HttpFileData::~HttpFileData()
{
    delete d;
}

QString HttpFileData::fileName() const
{
    return d->fileName;
}

void HttpFileData::setFileName(const QString &fileName)
{
    if (d->fileName == fileName) return;
    d->fileName = fileName;
    emit fileNameChanged(fileName);
}

QString HttpFileData::contentType() const
{
    return d->contentType;
}

void HttpFileData::setContentType(const QString &contentType)
{
    if (d->contentType == contentType) return;
    d->contentType = contentType;
    emit contentTypeChanged(contentType);
}

bool HttpFileData::save(const QString &fileName)
{
    bool ret = false;
    QFileInfo fi(fileName);
    if (!QDir::root().mkpath(fi.dir().absolutePath())) {
        qWarning() << fi.dir().absolutePath();
    } else {
        QFile file(d->file.fileName());
        if (!file.copy(fileName)) {
            qWarning() << file.errorString();
        } else {
            ret = true;
        }
    }
    return ret;
}

class HttpObject::Private
{
public:
    Private();

    QString remoteAddress;
    QString method;
    QString scheme;
    QString host;
    int port;
    QString path;
    QString query;
    QString data;
    QList<HttpFileData *> files;
    QVariant requestHeader;
    QVariantMap requestCookies;
    QString message;

    bool loading;
    int status;
    QVariantMap responseHeader;
    QVariantMap responseCookies;
    bool escapeHTML;
};

HttpObject::Private::Private()
    : port(-1)
    , loading(false)
    , status(200)
    , escapeHTML(false)
{

}

HttpObject::HttpObject(QObject *parent)
    : QObject(parent)
    , d(new Private)
{
}

HttpObject::~HttpObject()
{
    delete d;
}

QString HttpObject::remoteAddress() const
{
    return d->remoteAddress;
}

void HttpObject::setRemoteAddress(const QString &remoteAddress)
{
    if (d->remoteAddress == remoteAddress) return;
    d->remoteAddress = remoteAddress;
    emit remoteAddressChanged(remoteAddress);
}

QString HttpObject::method() const
{
    return d->method;
}

void HttpObject::setMethod(const QString &method)
{
    if (d->method == method) return;
    d->method = method;
    emit methodChanged(method);
}

QString HttpObject::scheme() const
{
    return d->scheme;
}

void HttpObject::setScheme(const QString &scheme)
{
    if (d->scheme == scheme) return;
    d->scheme = scheme;
    emit schemeChanged(scheme);
}

QString HttpObject::host() const
{
    return d->host;
}

void HttpObject::setHost(const QString &host)
{
    if (d->host == host) return;
    d->host = host;
    emit hostChanged(host);
}

int HttpObject::port() const
{
    return d->port;
}

void HttpObject::setPort(int port)
{
    if (d->port == port) return;
    d->port = port;
    emit portChanged(port);
}

QString HttpObject::path() const
{
    return d->path;
}

void HttpObject::setPath(const QString &path)
{
    if (d->path == path) return;
    d->path = path;
    emit pathChanged(path);
}

QString HttpObject::query() const
{
    return d->query;
}

void HttpObject::setQuery(const QString &query)
{
    if (d->query == query) return;
    d->query = query;
    emit queryChanged(query);
}

QString HttpObject::data() const
{
    return d->data;
}

void HttpObject::setData(const QString &data)
{
    if (d->data == data) return;
    d->data = data;
    emit dataChanged(data);
}

QQmlListProperty<HttpFileData> HttpObject::files()
{
    return QQmlListProperty<HttpFileData>(this, d->files);
}

void HttpObject::setFiles(const QList<HttpFileData *> &files)
{
    if (d->files == files) return;
    d->files = files;
    emit filesChanged(files);
}

QVariant HttpObject::requestHeader() const
{
    return d->requestHeader;
}

void HttpObject::setRequestHeader(const QVariant &requestHeader)
{
    if (d->requestHeader == requestHeader) return;
    d->requestHeader = requestHeader;
    emit requestHeaderChanged(requestHeader);
}

QVariantMap HttpObject::requestCookies() const
{
    return d->requestCookies;
}

void HttpObject::setRequestCookies(const QVariantMap &requestCookies)
{
    if (d->requestCookies == requestCookies) return;
    d->requestCookies = requestCookies;
    emit requestCookiesChanged(requestCookies);
}

QString HttpObject::message() const
{
    return d->message;
}

void HttpObject::setMessage(const QString &message)
{
    if (d->message == message) return;
    d->message = message;
    emit messageChanged(message);
}

bool HttpObject::isLoading() const
{
    return d->loading;
}

void HttpObject::setLoading(bool loading)
{
    if (d->loading == loading) return;
    d->loading = loading;
    emit loadingChanged(loading);
}

int HttpObject::status() const
{
    return d->status;
}

void HttpObject::setStatus(int status)
{
    if (d->status == status) return;
    d->status = status;
    emit statusChanged(status);
}

QVariantMap HttpObject::responseHeader() const
{
    return d->responseHeader;
}

void HttpObject::setResponseHeader(const QVariantMap &responseHeader)
{
    if (d->responseHeader == responseHeader) return;
    d->responseHeader = responseHeader;
    emit responseHeaderChanged(responseHeader);
}

QVariantMap HttpObject::responseCookies() const
{
    return d->responseCookies;
}

void HttpObject::setResponseCookies(const QVariantMap &responseCookies)
{
    if (d->responseCookies == responseCookies) return;
    d->responseCookies = responseCookies;
    emit responseCookiesChanged(responseCookies);
}

bool HttpObject::escapeHTML() const
{
    return d->escapeHTML;
}

void HttpObject::setEscapeHTML(bool escapeHTML)
{
    if (d->escapeHTML == escapeHTML) return;
    d->escapeHTML = escapeHTML;
    emit escapeHTMLChanged(escapeHTML);
}
