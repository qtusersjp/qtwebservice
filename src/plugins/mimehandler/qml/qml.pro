TARGET = qml

QT = webservice core-private

PLUGIN_TYPE = webservice-mimehandler
PLUGIN_CLASS_NAME = QmlPlugin
load(qt_plugin)

HEADERS += \
    qmlplugin.h \
    qmlhandler.h \
    httpobject.h \
    qwebserviceglobalobject.h \
    websocketobject.h \
    text.h

SOURCES += \
    qmlhandler.cpp \
    httpobject.cpp \
    qwebserviceglobalobject.cpp \
    websocketobject.cpp \
    text.cpp
