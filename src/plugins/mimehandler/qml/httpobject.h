/* Copyright (c) 2012 QtWebService Project.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the QtWebService nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL QTWEBSERVICE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HTTPOBJECT_H
#define HTTPOBJECT_H

#include <QtCore/QObject>
#include <QtCore/QUrl>

#include <QtQml/QQmlListProperty>

class QHttpFileData;

class HttpFileData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString fileName READ fileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString contentType READ contentType NOTIFY contentTypeChanged)
public:
    HttpFileData(QHttpFileData *data, QObject *parent = 0);
    ~HttpFileData();

    QString fileName() const;
    QString contentType() const;

    Q_INVOKABLE bool save(const QString &fileName);

private slots:
    void setFileName(const QString &fileName);
    void setContentType(const QString &contentType);

signals:
    void fileNameChanged(const QString &fileName);
    void contentTypeChanged(const QString &contentType);

private:
    class Private;
    Private *d;
    Q_DISABLE_COPY(HttpFileData)
};

class HttpObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString remoteAddress READ remoteAddress NOTIFY remoteAddressChanged)
    Q_PROPERTY(QString method READ method NOTIFY methodChanged)
    Q_PROPERTY(QString scheme READ scheme NOTIFY schemeChanged)
    Q_PROPERTY(QString host READ host NOTIFY hostChanged)
    Q_PROPERTY(int port READ port NOTIFY portChanged)
    Q_PROPERTY(QString path READ path NOTIFY pathChanged)
    Q_PROPERTY(QString query READ query NOTIFY queryChanged)
    Q_PROPERTY(QString data READ data NOTIFY dataChanged)
    Q_PROPERTY(QQmlListProperty<HttpFileData> files READ files)
    Q_PROPERTY(QVariant requestHeader READ requestHeader NOTIFY requestHeaderChanged)
    Q_PROPERTY(QVariantMap requestCookies READ requestCookies NOTIFY requestCookiesChanged)
    Q_PROPERTY(QString message READ message NOTIFY messageChanged)

    Q_PROPERTY(bool loading READ isLoading WRITE setLoading NOTIFY loadingChanged)
    Q_PROPERTY(int status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QVariantMap responseHeader READ responseHeader WRITE setResponseHeader NOTIFY responseHeaderChanged)
    Q_PROPERTY(QVariantMap responseCookies READ responseCookies WRITE setResponseCookies NOTIFY responseCookiesChanged)
    Q_PROPERTY(bool escapeHTML READ escapeHTML WRITE setEscapeHTML NOTIFY escapeHTMLChanged)
public:
    explicit HttpObject(QObject *parent = 0);
    ~HttpObject();

    QString remoteAddress() const;
    QString method() const;
    QString scheme() const;
    QString host() const;
    int port() const;
    QString path() const;
    QString query() const;
    QString data() const;
    QQmlListProperty<HttpFileData> files();
    QVariant requestHeader() const;
    QVariantMap requestCookies() const;
    QString message() const;

    bool isLoading() const;
    int status() const;
    QVariantMap responseHeader() const;
    QVariantMap responseCookies() const;
    bool escapeHTML() const;

public slots:
    void setRemoteAddress(const QString &remoteAddress);
    void setMethod(const QString &method);
    void setScheme(const QString &scheme);
    void setHost(const QString &host);
    void setPort(int port);
    void setPath(const QString &path);
    void setQuery(const QString &query);
    void setData(const QString &data);
    void setFiles(const QList<HttpFileData *> &files);
    void setRequestHeader(const QVariant &requestHeader);
    void setRequestCookies(const QVariantMap &requestCookies);
    void setMessage(const QString &message);

    void setLoading(bool loading);
    void setStatus(int status);
    void setResponseHeader(const QVariantMap &responseHeader);
    void setResponseCookies(const QVariantMap &responseCookies);
    void setEscapeHTML(bool escapeHTML);

signals:
    void remoteAddressChanged(const QString &remoteAddress);
    void methodChanged(const QString &method);
    void schemeChanged(const QString &scheme);
    void hostChanged(const QString &host);
    void portChanged(int port);
    void pathChanged(const QString &path);
    void queryChanged(const QString &query);
    void dataChanged(const QString &data);
    void filesChanged(const QList<HttpFileData *> &files);
    void requestHeaderChanged(const QVariant &requestHeader);
    void requestCookiesChanged(const QVariantMap &requestHeader);
    void messageChanged(const QString &message);

    void loadingChanged(bool loading);
    void statusChanged(int status);
    void responseHeaderChanged(const QVariantMap &responseHeader);
    void responseCookiesChanged(const QVariantMap &responseHeader);
    void escapeHTMLChanged(bool escapeHTML);

private:
    class Private;
    Private *d;
};

#endif // HTTPOBJECT_H
