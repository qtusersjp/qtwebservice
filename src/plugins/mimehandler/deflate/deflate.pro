TARGET = deflate

QT = webservice

PLUGIN_TYPE = webservice-mimehandler
PLUGIN_CLASS_NAME = DeflatePlugin
load(qt_plugin)

HEADERS += \
    deflateplugin.h \
    deflatehandler.h

SOURCES += \
    deflatehandler.cpp
