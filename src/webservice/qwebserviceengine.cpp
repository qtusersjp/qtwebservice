/* Copyright (c) 2012 QWebService Project.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the QWebService nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL QWEBSERVICE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "qwebserviceengine.h"
#include "qwebserviceconfig.h"
#include "qwebservice_logging_p.h"
#include "qwebservicemimehandlerplugin.h"
#include "qwebserviceabstractmimehandler.h"
#include "qwebserviceprotocolhandlerplugin.h"
#include "qwebserviceabstractprotocolhandler.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include <QtCore/QElapsedTimer>
#include <QtCore/QFile>
#include <QtCore/QMimeDatabase>
#include <QtCore/QRegularExpression>
#include <QtCore/QUrl>
#include <QtCore/private/qfactoryloader_p.h>

#include <QtHttpServer/QHttpRequest>
#include <QtHttpServer/QHttpReply>
#include <QtHttpServer/QWebSocket>

typedef QPair<QRegularExpression, QString> RewriteRule;


class QWebServiceEngine::Private : public QObject
{
    Q_OBJECT
public:
    Private(const QString &config, QWebServiceEngine *parent);

private slots:
    void incomingConnection(QHttpRequest *request, QHttpReply *reply);
    void incomingConnection(QWebSocket *socket);

private:
    template<typename T> void checkRewriteRule(T *connection) const;
    QString documentRootForRequest(const QUrl &url) const;
    void load(const QFileInfo &fileInfo, QHttpRequest *request, QHttpReply *reply, const QString &message = QString());
    void loadFile(const QFileInfo &fileInfo, QHttpRequest *request, QHttpReply *reply);
    void loadUrl(const QUrl &url, QHttpRequest *request, QHttpReply *reply, const QString &message = QString());
    void load(const QFileInfo &fileInfo, QWebSocket *socket, const QString &message = QString());
    void loadUrl(const QUrl &url, QWebSocket *socket, const QString &message = QString());
    void log(int statusCode, QHttpRequest *request, QHttpReply *reply, qint64 elapsed) const;

private slots:
    void error(int statusCode, QHttpRequest *request, QHttpReply *reply, const QString &message = QString());
    void error(int statusCode, QWebSocket *socket, const QString &message = QString());

private:
    QWebServiceEngine *q;
    QMimeDatabase mimeDatabase;
    QMap<QString, QWebServiceAbstractMimeHandler*> mimeHandlers;
    QMap<QString, QWebServiceAbstractProtocolHandler*> protocolHandlers;
    QList<RewriteRule> rewriteRules;
public:
    QHash<QString, QString> documentRoots;
};

QWebServiceEngine::Private::Private(const QString &config, QWebServiceEngine *parent)
    : QObject(parent)
    , q(parent)
{
    QWebServiceConfig::initialize(config);
    {
        QFactoryLoader mineHandlerLoader(QWebServiceMimeHandlerInterface_iid, QStringLiteral("/webservice-mimehandler"));
        auto size = mineHandlerLoader.metaData().length();
        for (int i = 0; i < size; i++) {
            QObject *object = mineHandlerLoader.instance(i);
            if (object) {
                QWebServiceMimeHandlerInterface *plugin = qobject_cast<QWebServiceMimeHandlerInterface *>(object);
                if (plugin) {
                    QWebServiceAbstractMimeHandler *handler = plugin->handler(this);
                    connect(handler, SIGNAL(error(int,QHttpRequest*,QHttpReply*,QString)), this, SLOT(error(int,QHttpRequest*,QHttpReply*,QString)));
                    connect(handler, SIGNAL(error(int,QWebSocket*,QString)), this, SLOT(error(int,QWebSocket*,QString)));
                    foreach (const QString &key, plugin->keys()) {
                        mimeHandlers.insert(key, handler);
                    }
                } else {
                    qWarning() << object;
                }
            } else {
                qWarning() << Q_FUNC_INFO << __LINE__;
            }
        }
    }
    {
        QFactoryLoader protocolHandlerLoader(QWebServiceProtocolHandlerInterface_iid, QStringLiteral("/webservice-protocolhandler"));
        auto size = protocolHandlerLoader.metaData().length();
        for (int i = 0; i < size; i++) {
            QObject *object = protocolHandlerLoader.instance(i);
            if (object) {
                QWebServiceProtocolHandlerInterface *plugin = qobject_cast<QWebServiceProtocolHandlerInterface *>(object);
                if (plugin) {
                    QWebServiceAbstractProtocolHandler *handler = plugin->handler(this);
                    foreach (const QString &key, plugin->keys()) {
                        protocolHandlers.insert(key, handler);
                    }
                } else {
                    qWarning() << object;
                }
            } else {
                qWarning() << Q_FUNC_INFO << __LINE__;
            }
        }
    }

    connect(q, SIGNAL(incomingConnection(QHttpRequest *, QHttpReply *)), this, SLOT(incomingConnection(QHttpRequest *, QHttpReply *)));
    connect(q, SIGNAL(incomingConnection(QWebSocket *)), this, SLOT(incomingConnection(QWebSocket *)));

    QUrl url = QWebServiceConfig::url();
    QVariantMap roots = QWebServiceConfig::value("contents").toMap();
    foreach (const QString &key, roots.keys()) {
        QString value = roots.value(key).toString();
        if (value.contains(":/")) {
            documentRoots.insert(key, value);
        } else {
            QFileInfo fileInfo(value);
            if (fileInfo.isRelative()) {
                if (url.scheme() == QStringLiteral("qrc")) {
                    QDir dir = QFileInfo(url.toString().mid(4)).dir();
                    dir.cd(value);
                    documentRoots.insert(key, QLatin1Char(':') + dir.absolutePath());
                } else {
                    QDir dir = QFileInfo(QWebServiceConfig::url().toLocalFile()).dir();
                    documentRoots.insert(key, QFileInfo(dir.absoluteFilePath(value)).canonicalFilePath());
                }
            } else {
                documentRoots.insert(key, value);
            }
        }
    }

//    qDebug() << documentRoots;

    QVariantMap rewrite = QWebServiceConfig::value("rewrite").toMap();
    foreach (const QString &key, rewrite.keys()) {
        QString value = rewrite.value(key).toString();
        rewriteRules.append(RewriteRule(QRegularExpression(key), value));
    }
}

QString QWebServiceEngine::Private::documentRootForRequest(const QUrl &url) const
{
    QString matched;
    foreach (const QString &key, documentRoots.keys()) {
        if (url.path().startsWith(key) && matched.length() < key.length()) {
            matched = key;
        }
    }

//    qDebug() << url << documentRoots.value(matched);
    return documentRoots.value(matched);
}

template<typename T> void QWebServiceEngine::Private::checkRewriteRule(T *connection) const
{
    QString str = connection->url().toString();
    foreach (const RewriteRule &rule, rewriteRules) {
        QRegularExpressionMatch match = rule.first.match(str);
        if (match.hasMatch()) {
            QString url = rule.second;
            for (int i = 0; i <= match.lastCapturedIndex(); i++) {
                url = url.replace(QString("$%1").arg(i), match.captured(i));
            }
            connection->setUrl(QUrl(url));
            break;
        }
    }
}

void QWebServiceEngine::Private::incomingConnection(QHttpRequest *request, QHttpReply *reply)
{
    QElapsedTimer timer;
    timer.start();
    qCDebug(QWS_LOG_REQUEST) << request;

    QVariant serverName = QWebServiceConfig::value("server.name");
    if (serverName.isNull()) {
        reply->setRawHeader("Server", "QtWebService");
    } else {
        QString str = serverName.toString();
        if (!str.isEmpty()) {
            reply->setRawHeader("Server", str.toUtf8());
        }
    }

    checkRewriteRule(request);

    QString documentRoot = documentRootForRequest(request->url());

    if (documentRoot.indexOf("://") > 0) {
        QUrl url(documentRoot);
        url.setPath(request->url().path());
        loadUrl(url, request, reply);
    } else {
        QString fileName(documentRoot + request->url().path());
        QFileInfo fileInfo(fileName);
        if (fileInfo.isDir()) {
            if (request->url().path().endsWith("/")) {
                fileName = fileName + QStringLiteral("/index.qml");
            } else {
                QUrl url(request->url());
                url.setPath(url.path() + "/");
                error(301, request, reply, url.toString());
                return;
            }
        }

        fileInfo = QFileInfo(fileName.replace(QRegularExpression("//+"), QStringLiteral("/")));
        if (fileInfo.exists()) {
            load(fileInfo, request, reply);
        } else {
            error(404, request, reply, request->url().toString());
            return;
        }
    }
    qCDebug(QWS_LOG_RESPONSE) << request << reply->size();
    log(200, request, reply, timer.elapsed());
}

void QWebServiceEngine::Private::incomingConnection(QWebSocket *socket)
{
    qCInfo(QWS_LOG_REQUEST) << socket;

    checkRewriteRule(socket);

    QString documentRoot = documentRootForRequest(socket->url());

    if (documentRoot.indexOf("://") > 0) {
        QUrl url(documentRoot);
        url.setPath(socket->url().path());
        loadUrl(url, socket);
    } else {
        QString fileName(documentRoot + socket->url().path());
        QFileInfo fileInfo(fileName);
        if (fileInfo.isDir()) {
            if (socket->url().path().endsWith("/")) {
                fileName = fileName + QStringLiteral("/index.qml");
            } else {
                QUrl url(socket->url());
                url.setPath(url.path() + "/");
                error(301, socket, url.toString());
                return;
            }
        }

        fileInfo = QFileInfo(fileName.replace(QRegularExpression("//+"), QStringLiteral("/")));
        if (fileInfo.exists()) {
            load(fileInfo, socket);
        } else {
            error(404, socket, socket->url().toString());
            return;
        }
    }
    qCInfo(QWS_LOG_RESPONSE) << socket;
}

void QWebServiceEngine::Private::log(int statusCode, QHttpRequest *request, QHttpReply *reply, qint64 elapsed) const
{
    QString ip = request->hasRawHeader("X-Forwarded-For") ? QString::fromUtf8(request->rawHeader("X-Forwarded-For")) : request->remoteAddress();
    qCInfo(QWS_LOG).nospace()
            << "{ \"timestamp\": " << QDateTime::currentDateTime().toString()
            << ", \"uuid\": " << request->uuid().toString()
            << ", \"method\": " << request->method()
            << ", \"url\": " << request->url().toString()
            << ", \"request_data_size\": " << request->size()
            << ", \"ip\": " << ip
            << ", \"user_agent\": " << QString::fromUtf8(request->rawHeader("User-Agent"))
            << ", \"referer\": " << QString::fromUtf8(request->rawHeader("Referer"))
            << ", \"reply_data_size\": " << reply->size()
            << ", \"status_code\": " << statusCode
            << ", \"elapsed\": " << elapsed
            << " }";
}

void QWebServiceEngine::Private::load(const QFileInfo &fileInfo, QHttpRequest *request, QHttpReply *reply, const QString &message)
{
    QStringList mimeTypeListNeedCharset;
    mimeTypeListNeedCharset << QStringLiteral("text/x-qml")
                            << QStringLiteral("text/css")
                            << QStringLiteral("text/html")
                            << QStringLiteral("text/plain")
                            << QStringLiteral("image/svg+xml")
                            << QStringLiteral("application/javascript")
                            << QStringLiteral("application/x-javascript")
                            << QStringLiteral("application/xml")
                            << QStringLiteral("application/atom")
                            << QStringLiteral("application/rss+xml")
                            << QStringLiteral("application/x-shockwave-flash");
    QMimeType mimeType = mimeDatabase.mimeTypeForFile(fileInfo.fileName(), QMimeDatabase::MatchExtension);
    QString mime = mimeType.name();
    QString contentType = mime;
    if (mimeTypeListNeedCharset.contains(mime)) {
        contentType.append(QStringLiteral("; charset=utf-8"));
    }
    reply->setRawHeader("Content-Type", contentType.toUtf8());
    if (!mimeHandlers.contains(mime)) {
        mime = mime.section(QLatin1Char('/'), 0, 0) + QStringLiteral("/*");
    }
    if (mimeHandlers.contains(mime)) {
        QUrl url;
        if (fileInfo.filePath().startsWith(":/")) {
            url = QUrl("qrc" + fileInfo.absoluteFilePath());
        } else {
            url = QUrl::fromLocalFile(fileInfo.absoluteFilePath());
        }
        bool ret = mimeHandlers[mime]->load(url, request, reply, message);
        if (!ret) {
            loadFile(fileInfo, request, reply);
        }
    } else {
        loadFile(fileInfo, request, reply);
    }
}

void QWebServiceEngine::Private::loadFile(const QFileInfo &fileInfo, QHttpRequest *request, QHttpReply *reply)
{
    if (fileInfo.fileName().startsWith(".")) {
        error(403, request, reply, request->url().toString());
    } else {
        // TODO cache
        QFile file(fileInfo.absoluteFilePath());
        if (file.open(QFile::ReadOnly)) {
            QDateTime lastModified = fileInfo.lastModified();
            if (lastModified.isNull()) {
                lastModified.setTime_t(0);
            }
            reply->setRawHeader("Last-Modified", lastModified.toUTC().toString("ddd, d, MMM yyyy hh:mm:ss UTC").toUtf8());
            while (!file.atEnd()) {
                reply->write(file.read(1024 * 1024));
            }
            file.close();
            reply->close();
        } else {
            error(403, request, reply, request->url().toString());
        }
    }
}

void QWebServiceEngine::Private::loadUrl(const QUrl &url, QHttpRequest *request, QHttpReply *reply, const QString &message)
{
    bool ret = false;
    if (protocolHandlers.contains(url.scheme())) {
        ret = protocolHandlers[url.scheme()]->load(url, request, reply, message);
    }
    if (!ret) {
        error(403, request, reply, request->url().toString());
    }
}

void QWebServiceEngine::Private::error(int statusCode, QHttpRequest *request, QHttpReply *reply, const QString &message)
{
    qCWarning(QWS_LOG_RESPONSE) << request << statusCode << message;
    QString documentRoot = documentRootForRequest(request->url());
    if (QFile::exists(QString::fromUtf8("%1/errors/%2.qml").arg(documentRoot).arg(statusCode))) {
        load(QFileInfo(QString::fromUtf8("%1/errors/%2.qml").arg(documentRoot).arg(statusCode)), request, reply, message);
    } else if (QFile::exists(QString::fromUtf8(":/errors/%2.qml").arg(statusCode))) {
        load(QFileInfo(QString::fromUtf8(":/errors/%2.qml").arg(statusCode)), request, reply, message);
    } else {
        reply->setStatus(statusCode);
        load(QFileInfo(QString::fromUtf8(":/qtwebservice/error.qml")), request, reply, message);
    }
    log(statusCode, request, reply, -1);
}

void QWebServiceEngine::Private::load(const QFileInfo &fileInfo, QWebSocket *socket, const QString &message)
{
    QMimeType mimeType = mimeDatabase.mimeTypeForFile(fileInfo.fileName(), QMimeDatabase::MatchExtension);
    QString mime = mimeType.name();
    if (mimeHandlers.contains(mime)) {
        QUrl url;
        if (fileInfo.filePath().startsWith(":/")) {
            url = QUrl("qrc" + fileInfo.absoluteFilePath());
        } else {
            url = QUrl::fromLocalFile(fileInfo.absoluteFilePath());
        }
        bool ret = mimeHandlers[mime]->load(url, socket, message);
        if (!ret) {
            error(401, socket, socket->url().toString());
        }
    } else {
        error(401, socket, socket->url().toString());
    }
}

void QWebServiceEngine::Private::loadUrl(const QUrl &url, QWebSocket *socket, const QString &message)
{
    bool ret = false;
    if (protocolHandlers.contains(url.scheme())) {
        ret = protocolHandlers[url.scheme()]->load(url, socket, message);
    }
    if (!ret) {
        error(403, socket, socket->url().toString());
    }
}

void QWebServiceEngine::Private::error(int statusCode, QWebSocket *socket, const QString &message)
{
    qCWarning(QWS_LOG_RESPONSE) << socket << statusCode << message;

    // TODO: error handling
//    socket->close();
    socket->deleteLater();
}

QWebServiceEngine::QWebServiceEngine(const QString &config, QObject *parent)
    : QHttpServer(parent)
    , d(new Private(config, this))
{
}

void QWebServiceEngine::start()
{
    QString listenAddress = QWebServiceConfig::value("listen.address").toString();
    QHostAddress address;
    if (listenAddress == QStringLiteral("*")) {
        address = QHostAddress::Any;
    } else if (listenAddress == QStringLiteral("localhost")) {
        address = QHostAddress::LocalHost;
    } else if (!address.setAddress(listenAddress)) {
        qWarning() << "The address" << listenAddress << "is not available.";
        return;
    }

    int port = QWebServiceConfig::value("listen.port").toInt();

    if (listen(address, port)) {
        qCInfo(QWS_LOG) << "qwebservice is running on" << port;
    } else {
        qCWarning(QWS_LOG) << errorString();
    }
}

#include "qwebserviceengine.moc"
