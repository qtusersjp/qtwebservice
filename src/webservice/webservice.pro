TARGET     = QtWebService
MODULE     = webservice
QT         = qml httpserver core-private

MODULE_PLUGIN_TYPES += \
    webservice-mimehandler \
    webservice-protocolhandler \
    webservice-imports

HEADERS += \
    qtwebserviceglobal.h \
    qwebserviceconfig.h \
    qwebserviceabstracthttpobject.h \
    qwebservicemimehandlerplugin.h \
    qwebserviceabstractmimehandler.h \
    qwebserviceprotocolhandlerplugin.h \
    qwebserviceabstractprotocolhandler.h \
    qwebserviceabstractobject.h \
    qwebservice_logging_p.h \
    qwebserviceimportsplugin.h \
    qwebserviceengine.h

SOURCES += \
    qwebserviceconfig.cpp \
    qwebserviceabstracthttpobject.cpp \
    qwebserviceabstractmimehandler.cpp \
    qwebserviceabstractprotocolhandler.cpp \
    qwebserviceabstractobject.cpp \
    qwebservice_logging.cpp \
    qwebserviceengine.cpp

load(qt_module)

CONFIG -= create_cmake

RESOURCES += \
    webservice.qrc
