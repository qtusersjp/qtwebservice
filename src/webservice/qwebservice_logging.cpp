#include "qwebservice_logging_p.h"

Q_LOGGING_CATEGORY(QWS_LOG, "qt.webservice.server")
Q_LOGGING_CATEGORY(QWS_LOG_REQUEST, "qt.webservice.server.request")
Q_LOGGING_CATEGORY(QWS_LOG_RESPONSE, "qt.webService.server.response")
