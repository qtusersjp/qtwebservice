#include "qwebserviceconfig.h"

#include <QtCore/QCommandLineParser>
#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>

#include <QtQml/QQmlEngine>
#include <QtQml/QQmlComponent>
#include <QtQml/QQmlContext>

QVariantHash QWebServiceConfig::m_config;
QUrl QWebServiceConfig::m_url;

class QWebServiceServerConfigListen : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString address MEMBER address NOTIFY addressChanged)
    Q_PROPERTY(int port MEMBER port NOTIFY portChanged)
public:
    explicit QWebServiceServerConfigListen(QObject *parent = 0) : QObject(parent), address(QStringLiteral("localhost")), port(8080) {}

signals:
    void addressChanged(const QString &address);
    void portChanged(int port);

private:
    QString address;
    int port;
};

class QWebServiceServerConfigServer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER name NOTIFY nameChanged)
public:
    explicit QWebServiceServerConfigServer(QObject *parent = 0) : QObject(parent), name(QStringLiteral("QtWebService")) {}

signals:
    void nameChanged(const QString &name);

private:
    QString name;
};

class QWebServiceServerConfig : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QWebServiceServerConfigListen *listen MEMBER listen NOTIFY listenChanged)
    Q_PROPERTY(QWebServiceServerConfigServer *server MEMBER server NOTIFY serverChanged)
    Q_PROPERTY(QJsonObject contents MEMBER contents NOTIFY contentsChanged)
    Q_PROPERTY(QJsonArray daemons MEMBER daemons NOTIFY daemonsChanged)
    Q_PROPERTY(QJsonArray imports MEMBER imports NOTIFY importsChanged)
    Q_PROPERTY(QString offlineStoragePath MEMBER offlineStoragePath NOTIFY offlineStoragePathChanged)
    Q_PROPERTY(QJsonObject cache MEMBER cache NOTIFY cacheChanged)
    Q_PROPERTY(QJsonObject deflate MEMBER deflate NOTIFY deflateChanged)
    Q_PROPERTY(QJsonObject rewrite MEMBER rewrite NOTIFY rewriteChanged)
public:
    explicit QWebServiceServerConfig(QObject *parent = 0);

signals:
    void listenChanged(const QWebServiceServerConfigListen *listen);
    void serverChanged(const QWebServiceServerConfigServer *server);
    void contentsChanged(const QJsonObject &contents);
    void daemonsChanged(const QJsonArray &daemons);
    void importsChanged(const QJsonArray &imports);
    void offlineStoragePathChanged(const QString &offlineStoragePath);
    void cacheChanged(const QJsonObject &cache);
    void deflateChanged(const QJsonObject &deflate);
    void rewriteChanged(const QJsonObject &rewrite);

private:
    QWebServiceServerConfigListen *listen;
    QWebServiceServerConfigServer *server;
    QJsonObject contents;
    QJsonArray daemons;
    QJsonArray imports;
    QString offlineStoragePath;
    QJsonObject cache;
    QJsonObject deflate;
    QJsonObject rewrite;
};

QWebServiceServerConfig::QWebServiceServerConfig(QObject *parent)
    : QObject(parent)
    , listen(new QWebServiceServerConfigListen(this))
    , server(new QWebServiceServerConfigServer(this))
{
}

const QVariantHash &QWebServiceConfig::config()
{
    return m_config;
}

QVariant QWebServiceConfig::value(const QVariant &v, const QString &key)
{
    QVariantHash hash;
    switch (v.type()) {
    case QVariant::Hash:
        hash = v.toHash();
        break;
    case QVariant::Map: {
        QMap<QString, QVariant> map = v.toMap();
        foreach (const QString &k, map.keys())
            hash.insert(k, map.value(k));
        break; }
    case QMetaType::QJsonObject: {
        QJsonObject object = v.toJsonObject();
        foreach (const QString &k, object.keys())
            hash.insert(k, object.value(k));
        break; }
    default:
        qWarning() << v << "not supported";
        break;
    }

    int index = key.indexOf(QLatin1Char('.'));
    if (index > 0) {
        if(!hash.contains(key.left(index))) {
            qWarning() << "No config value found for" << key.left(index);
            return QVariant();
        }
        return value(hash.value(key.left(index)), key.mid(index + 1));
    }
    if (!hash.contains(key)) {
        qWarning() << "No config value found for" << key;
        return QVariant();
    }
    return hash.value(key);
}

QVariant QWebServiceConfig::value(const QString &key)
{
    int index = key.indexOf(QLatin1Char('.'));
    if (index > 0) {
        QVariant ret = value(m_config.value(key.left(index)), key.mid(index + 1));
        return ret;
    }
    return m_config.value(key);
}

void QWebServiceConfig::initialize(const QString &config)
{
    QString fileName = config;

    QCommandLineParser commandLine;
    QCommandLineOption configOption(QStringLiteral("config"), QStringLiteral("configuration qml file"), QStringLiteral("config.qml"));
    commandLine.addOption(configOption);
    QCommandLineOption addressOption(QStringLiteral("listen.address"), QStringLiteral("listening address"), QStringLiteral("*"));
    commandLine.addOption(addressOption);
    QCommandLineOption portOption(QStringLiteral("listen.port"), QStringLiteral("listening port"), QStringLiteral("8080"));
    commandLine.addOption(portOption);
    commandLine.addHelpOption();
    commandLine.process(*QCoreApplication::instance());

    if (commandLine.isSet(configOption)) {
        QString userFileName = commandLine.value(QStringLiteral("config"));
        if (!QFile::exists(userFileName)) {
            qWarning() << userFileName << "not found." << QDir::currentPath();
        } else {
            fileName = userFileName;
        }
    }

    if (QFile::exists(fileName)) {
        QUrl url;
        if (fileName.startsWith(QStringLiteral(":/")))
            url = QUrl(QStringLiteral("qrc%1").arg(fileName));
        else
            url = QUrl::fromLocalFile(QFileInfo(QDir::current().absoluteFilePath(fileName)).canonicalFilePath());
        QVariantHash override = readConfigFile(url);
        foreach (const QString &key, override.keys()) {
            if (m_config.contains(key)) {
                if (override.value(key).isNull()) {
                    qWarning() << "Configuration:" << key << "is ignored because of no value.";
                    qWarning() << "Expected:" << m_config.value(key);
                    continue;
                }
                if (m_config.value(key).type() != override.value(key).type()) {
                    qWarning() << "Configuration:" << key << "is ignored because of type mismatch.";
                    qWarning() << "Expected:" << m_config.value(key).type() << "Actual:" << override.value(key).type();
                    continue;
                }
            }
            m_config.insert(key, override.value(key));
        }
    }

    QVariantHash listen = m_config.value(QLatin1String("listen")).toHash();
    if (commandLine.isSet(addressOption)) {
        listen.insert(QStringLiteral("address"), commandLine.value(addressOption));
    }
    if (commandLine.isSet(portOption)) {
        listen.insert(QStringLiteral("port"), commandLine.value(portOption).toInt());
    }
    m_config.insert(QStringLiteral("listen"), listen);
}

static QVariantHash readObject(const QObject *object)
{
    QVariantHash ret;

    const QMetaObject *metaObject = object->metaObject();
    for (int i = 0; i < metaObject->propertyCount(); i++) {
        QMetaProperty property = metaObject->property(i);
        QString key = QString::fromUtf8(property.name());
        QVariant value = property.read(object);
        QObject *child = qvariant_cast<QObject *>(value);
        if (child) {
            ret.insert(key, readObject(child));
        } else {
            if (value.canConvert<QJSValue>()) {
                ret.insert(key, value.value<QJSValue>().toVariant());
            } else {
                ret.insert(key, value);
            }
        }
    }
    metaObject = &object->staticMetaObject;
    for (int i = 0; i < metaObject->propertyCount(); i++) {
        ret.remove(metaObject->property(i).name());
    }

    delete object;
    return ret;
}

QVariantHash QWebServiceConfig::readConfigFile(const QUrl &fileName)
{
    QVariantHash ret;
    m_url = fileName;

    QQmlEngine engine;
    qmlRegisterType<QWebServiceServerConfigListen>("QtWebService.Config", 0, 1, "Listen");
    qmlRegisterType<QWebServiceServerConfigServer>("QtWebService.Config", 0, 1, "Server");
    qmlRegisterType<QWebServiceServerConfig>("QtWebService.Config", 0, 1, "Config");
    QQmlComponent component(&engine, m_url);
    QObject *object = component.create();
    if (component.status() == QQmlComponent::Ready) {
        ret = readObject(object);
    } else {
        qCritical().noquote() << component.errorString();
        qFatal("***Fix the error above***");
    }
    engine.clearComponentCache();
//    qmlClearTypeRegistrations();

    return ret;
}

QUrl QWebServiceConfig::url()
{
    return m_url;
}

#include "qwebserviceconfig.moc"
