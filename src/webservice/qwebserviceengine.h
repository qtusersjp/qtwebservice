#ifndef QWEBSERVICEENGINE_H
#define QWEBSERVICEENGINE_H

#include "qtwebserviceglobal.h"

#include <QtHttpServer/QHttpServer>

class Q_WEBSERVICE_EXPORT QWebServiceEngine : public QHttpServer
{
    Q_OBJECT
public:
    explicit QWebServiceEngine(const QString &config, QObject *parent = Q_NULLPTR);

public Q_SLOTS:
    void start();

private:
    class Private;
    Private *d;
    Q_DISABLE_COPY(QWebServiceEngine)
};

#endif // QWEBSERVICEENGINE_H
