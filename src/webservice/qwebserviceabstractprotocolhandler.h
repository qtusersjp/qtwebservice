/* Copyright (c) 2012 QWebService Project.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the QWebService nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL QWEBSERVICE BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QWEBSERVICEABSTRACTPROTOCOLHANDLER_H
#define QWEBSERVICEABSTRACTPROTOCOLHANDLER_H

#include <QtCore/QObject>
#include <QtCore/QFileInfo>

#include "qtwebserviceglobal.h"

class QHttpRequest;
class QHttpReply;
class QWebSocket;

class Q_WEBSERVICE_EXPORT QWebServiceAbstractProtocolHandler : public QObject
{
    Q_OBJECT
public:
    explicit QWebServiceAbstractProtocolHandler(QObject *parent = Q_NULLPTR);
    
    virtual bool load(const QUrl &url, QHttpRequest *request, QHttpReply *reply, const QString &message = QString()) = 0;
    virtual bool load(const QUrl &url, QWebSocket *socket, const QString &message = QString()) { Q_UNUSED(url) Q_UNUSED(socket) Q_UNUSED(message) return false; }

Q_SIGNALS:
    void error(int code, QHttpRequest *request, QHttpReply *reply, const QString &errorString = QString());

private:
    Q_DISABLE_COPY(QWebServiceAbstractProtocolHandler)
};

#endif // QWEBSERVICEABSTRACTPROTOCOLHANDLER_H
