#ifndef QWEBSERVICECONFIG_H
#define QWEBSERVICECONFIG_H

#include "qtwebserviceglobal.h"

#include <QtCore/QVariant>

class Q_WEBSERVICE_EXPORT QWebServiceConfig
{
public:
    static void initialize(const QString &config);
    static const QVariantHash &config();
    static QVariant value(const QString &key);
    static QUrl url();

private:
    QWebServiceConfig() {}

    static QVariantHash readConfigFile(const QUrl &url);
    static QVariant value(const QVariant &v, const QString &key);

    static QVariantHash m_config;
    static QUrl m_url;
};

#endif // QWEBSERVICECONFIG_H
